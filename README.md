# Web/iOS Application - Stock Search (JavaScript, PHP, Objective-C, AWS) #

### AutoComplete With Related Result ###

![alt tag](https://bytebucket.org/twistedlogicusc/stock_search/raw/ce995df48a7342b8025aa11ace2dcd73c49b17fc/screenshot1.png)

### Result Page ###

![alt tag](https://bytebucket.org/twistedlogicusc/stock_search/raw/ce995df48a7342b8025aa11ace2dcd73c49b17fc/screenshot2.png)

###  News Headlines ###

![alt tag](https://bytebucket.org/twistedlogicusc/stock_search/raw/ce995df48a7342b8025aa11ace2dcd73c49b17fc/screenshot3.png)